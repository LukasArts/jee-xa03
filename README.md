# Taxi booker Web Service
This implements a SOAP web service responding with a quote to a request for a taxi ride.
In a spirit of _Configuration by exception_ endorsed in Java EE 7 there is no need for any configuration in xml files as everything has been set up using annotations.

## Client
Demonstrates service functionality.
### Usage
`java TaxiBookerDemo origin destination number of passengers`
#### Example
    java TaxiBookerDemo Michego "San Bonigo" 6
    AAA 833,76

## Web Service
Service responds to SOAP requests with SOAP responses.

SOAP Request
````xml
<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <S:Body>
        <ns2:getFareQuote xmlns:ns2="http://ws.taxi.ucl.cz/">
            <pick-up>Michego</pick-up>
            <drop-off>San Bonigo</drop-off>
            <number_of_passengers>3</number_of_passengers>
        </ns2:getFareQuote>
    </S:Body>
</S:Envelope>
````

SOAP Response
````xml
<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <S:Body>
        <ns2:getFareQuoteResponse xmlns:ns2="http://ws.taxi.ucl.cz/">
            <return>
                <farePrice>450.301207395987</farePrice>
                <taxiCompany>HaloTaxi</taxiCompany>
            </return>
        </ns2:getFareQuoteResponse>
    </S:Body>
</S:Envelope>
````

Message parameters as described in WSDL
````xml
<xs:schema xmlns:tns="http://ws.taxi.ucl.cz/" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="1.0" targetNamespace="http://ws.taxi.ucl.cz/">
<xs:element name="getFareQuote" type="tns:getFareQuote"/>
<xs:element name="getFareQuoteResponse" type="tns:getFareQuoteResponse"/>
<xs:complexType name="getFareQuote">
    <xs:sequence>
        <xs:element name="pick-up" type="xs:string" minOccurs="0"/>
        <xs:element name="drop-off" type="xs:string" minOccurs="0"/>
        <xs:element name="number_of_passengers" type="xs:int" minOccurs="0"/>
    </xs:sequence>
</xs:complexType>
<xs:complexType name="getFareQuoteResponse">
    <xs:sequence>
        <xs:element name="return" type="tns:taxiResponse" minOccurs="0"/>
    </xs:sequence>
</xs:complexType>
<xs:complexType name="taxiResponse">
    <xs:sequence>
        <xs:element name="farePrice" type="xs:double" minOccurs="0"/>
        <xs:element name="taxiCompany" type="xs:string" minOccurs="0"/>
    </xs:sequence>
</xs:complexType>
</xs:schema>
````