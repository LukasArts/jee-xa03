package cz.ucl.taxi.client;

public class TaxiBookerDemo {

    public static void main(String[] argv) {
        String usage = "Finds cab available for fare from source to destination.\n" +
                "Usage:\tjava TaxiBookerDemo origin destination number of passengers\n";
        try {
            if (argv.length != 3)
                throw new IllegalArgumentException("You need to provide origin, destination and number of passengers.");

            String origin = argv[0];
            String destination = argv[1];
            Integer passengers = Integer.valueOf(argv[2]);

            TaxiBookerService portFactory = new TaxiBookerService();
            TaxiBooker service = portFactory.getTaxiBookerPort();

            TaxiResponse response = service.getFareQuote(origin, destination, passengers);
            System.out.println(response.getTaxiCompany() + " " + String.format("%.2f", response.getFarePrice()));
        } catch (IllegalArgumentException e) {
            System.out.println(usage);
            if (argv.length != 1) System.err.println(e.getMessage());
        }

    }

}
