
package cz.ucl.taxi.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cz.ucl.taxi.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetFareQuote_QNAME = new QName("http://ws.taxi.ucl.cz/", "getFareQuote");
    private final static QName _GetFareQuoteResponse_QNAME = new QName("http://ws.taxi.ucl.cz/", "getFareQuoteResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cz.ucl.taxi.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetFareQuote }
     * 
     */
    public GetFareQuote createGetFareQuote() {
        return new GetFareQuote();
    }

    /**
     * Create an instance of {@link GetFareQuoteResponse }
     * 
     */
    public GetFareQuoteResponse createGetFareQuoteResponse() {
        return new GetFareQuoteResponse();
    }

    /**
     * Create an instance of {@link TaxiResponse }
     * 
     */
    public TaxiResponse createTaxiResponse() {
        return new TaxiResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFareQuote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.taxi.ucl.cz/", name = "getFareQuote")
    public JAXBElement<GetFareQuote> createGetFareQuote(GetFareQuote value) {
        return new JAXBElement<GetFareQuote>(_GetFareQuote_QNAME, GetFareQuote.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFareQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.taxi.ucl.cz/", name = "getFareQuoteResponse")
    public JAXBElement<GetFareQuoteResponse> createGetFareQuoteResponse(GetFareQuoteResponse value) {
        return new JAXBElement<GetFareQuoteResponse>(_GetFareQuoteResponse_QNAME, GetFareQuoteResponse.class, null, value);
    }

}
