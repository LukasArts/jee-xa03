
package cz.ucl.taxi.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFareQuote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFareQuote"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pick-up" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="drop-off" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="number_of_passengers" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFareQuote", propOrder = {
        "pickUp",
        "dropOff",
        "numberOfPassengers"
})
public class GetFareQuote {

    @XmlElement(name = "pick-up")
    protected String pickUp;
    @XmlElement(name = "drop-off")
    protected String dropOff;
    @XmlElement(name = "number_of_passengers")
    protected Integer numberOfPassengers;

    /**
     * Gets the value of the pickUp property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickUp() {
        return pickUp;
    }

    /**
     * Sets the value of the pickUp property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickUp(String value) {
        this.pickUp = value;
    }

    /**
     * Gets the value of the dropOff property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDropOff() {
        return dropOff;
    }

    /**
     * Sets the value of the dropOff property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDropOff(String value) {
        this.dropOff = value;
    }

    /**
     * Gets the value of the numberOfPassengers property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfPassengers() {
        return numberOfPassengers;
    }

    /**
     * Sets the value of the numberOfPassengers property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfPassengers(Integer value) {
        this.numberOfPassengers = value;
    }

}
