
package cz.ucl.taxi.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for taxiResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="taxiResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="farePrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="taxiCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taxiResponse", propOrder = {
        "farePrice",
        "taxiCompany"
})
public class TaxiResponse {

    protected Double farePrice;
    protected String taxiCompany;

    /**
     * Gets the value of the farePrice property.
     *
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getFarePrice() {
        return farePrice;
    }

    /**
     * Sets the value of the farePrice property.
     *
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setFarePrice(Double value) {
        this.farePrice = value;
    }

    /**
     * Gets the value of the taxiCompany property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxiCompany() {
        return taxiCompany;
    }

    /**
     * Sets the value of the taxiCompany property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxiCompany(String value) {
        this.taxiCompany = value;
    }

}
