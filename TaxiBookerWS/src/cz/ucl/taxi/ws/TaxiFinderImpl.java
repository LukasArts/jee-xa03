package cz.ucl.taxi.ws;

import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import java.util.concurrent.ThreadLocalRandom;

@Priority(100)
@Alternative
@RequestScoped
public class TaxiFinderImpl implements TaxiFinder {
    @Override
    public TaxiResponse calculateFare(TaxiRequest request) {
        TaxiResponse result = new TaxiResponse();

        result.setFarePrice(Double.MAX_VALUE);
        Double distance = getDistance(request.getRouteOrigin(), request.getRouteDestination());
        System.out.println(request.getRouteOrigin() + " -> " + request.getRouteDestination() + "(" + distance + " km)");

        for (TaxiCompany t : TaxiCompany.values()) {
            Double price = t.getFare(distance);
            if (price < result.getFarePrice() && request.getNumberOfPassengers() < t.maxPassengers) {
                result.setFarePrice(price);
                result.setTaxiCompany(t.name());
            }
        }
        return result;
    }

    private Double getDistance(String origin, String destination) {
        Short minDistance = 1;
        Short maxDistance = 100;
        Double distance = ThreadLocalRandom.current().nextDouble(minDistance, maxDistance);
        System.out.println("Faked distance " + distance + " from " + origin + " to " + destination);
        return distance;
    }

    private enum TaxiCompany {
        AAA(45.0, 30.0, 15),
        SpeedCars(50.0, 40.0, 10),
        TickTack(39.0, 35.0, 4),
        NejlevnejsiTaxi(25.0, 30.0, 3),
        HaloTaxi(17.0, 50.0, 6);

        private final Double perKmCharge;
        private final Double initialCharge;
        private final Integer maxPassengers;

        TaxiCompany(Double perKmCharge, Double initialCharge, Integer maxPassengers) {
            this.perKmCharge = perKmCharge;
            this.initialCharge = initialCharge;
            this.maxPassengers = maxPassengers;
        }

        Double getFare(Double distance) {
            return initialCharge + this.perKmCharge * distance;
        }

        Integer getMaxPassengers() {
            return maxPassengers;
        }

    }
}
