package cz.ucl.taxi.ws;

import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;

@Priority(50)
@Alternative
@RequestScoped
public class TaxiFinderMock implements TaxiFinder {
    @Override
    public TaxiResponse calculateFare(TaxiRequest request) {
        TaxiResponse taxiResponse = new TaxiResponse();
        taxiResponse.setTaxiCompany("UCL taxi");
        taxiResponse.setFarePrice(123.45);
        return taxiResponse;
    }
}
