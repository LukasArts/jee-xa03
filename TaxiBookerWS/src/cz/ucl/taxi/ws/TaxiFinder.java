package cz.ucl.taxi.ws;

public interface TaxiFinder {
    TaxiResponse calculateFare(TaxiRequest request);
}
