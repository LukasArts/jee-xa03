package cz.ucl.taxi.ws;

public class TaxiRequest {
    private String routeOrigin;
    private String routeDestination;
    private Integer numberOfPassengers;

    public TaxiRequest(String routeOrigin, String routeDestination, Integer numberOfPassengers) {
        this.routeOrigin = routeOrigin;
        this.routeDestination = routeDestination;
        this.numberOfPassengers = numberOfPassengers;
    }

    public String getRouteOrigin() {
        return routeOrigin;
    }

    public void setRouteOrigin(String routeOrigin) {
        this.routeOrigin = routeOrigin;
    }

    public String getRouteDestination() {
        return routeDestination;
    }

    public Integer getNumberOfPassengers() {
        return numberOfPassengers;
    }
}