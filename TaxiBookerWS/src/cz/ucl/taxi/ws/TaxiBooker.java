package cz.ucl.taxi.ws;

import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class TaxiBooker {

    @Inject
    TaxiFinder taxiFinder;

    public TaxiResponse getFareQuote(@WebParam(name = "pick-up") String origin,
                                     @WebParam(name = "drop-off") String destination,
                                     @WebParam(name = "number_of_passengers") Integer passengers) {
        TaxiRequest taxiRequest = new TaxiRequest(origin, destination, passengers);
        TaxiResponse taxiResponse = taxiFinder.calculateFare(taxiRequest);
        System.out.println("Got quote for " + taxiResponse.getFarePrice() + " from " + taxiResponse.getTaxiCompany());
        return taxiResponse;
    }

}
