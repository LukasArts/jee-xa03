package cz.ucl.taxi.ws;

public class TaxiResponse {
    private String taxiCompany;
    private Double farePrice;

    public String getTaxiCompany() {
        return taxiCompany;
    }

    public void setTaxiCompany(String taxiCompany) {
        this.taxiCompany = taxiCompany;
    }

    public Double getFarePrice() {
        return farePrice;
    }

    public void setFarePrice(Double farePrice) {
        this.farePrice = farePrice;
    }
}

